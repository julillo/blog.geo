var sourceLat = -33.456;
var sourceLon = -70.555;

var sourceRadio = 0;
var sourceHeader1 = "Usted esta aquí";
//var sourceHeader2 = "__source_header2__";
//var sourceHeader3 = "__source_header3__";

//var targetLat = -33.466;
//var targetLon = -70.565;
//var targetHeader1 = "__target_header1__";
//var targetHeader2 = "__target_header2__";
//var targetHeader3 = "__target_header3__";

var iconBase = 'https://maps.google.com/';
var icons = {
    parking: {
        icon: iconBase + 'mapfiles/kml/shapes/parking_lot_maps.png'
    },
    library: {
        icon: iconBase + 'mapfiles/kml/shapes/library_maps.png'
    },
    info: {
        icon: iconBase + 'mapfiles/kml/shapes/info-i_maps.png'
    },
    tree: {
        icon: iconBase + 'intl/en_us/mapfiles/ms/micons/tree.png'
    }
};

var sourceLatLng = new google.maps.LatLng(sourceLat, sourceLon);  // Default to Hollywood, CA when no geolocation support
//var targetLatLng = new google.maps.LatLng(targetLat, targetLon);  // Default to Hollywood, CA when no geolocation support

if (sourceAutomatic && navigator.geolocation) {
    function success(pos) {
        // Location found, show map with these coordinates
        drawMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude), posts);
    }

    function fail(error) {
        drawMap(sourceLatLng, posts);  // Failed to find location, show default map
    }

    // Find the users current position.  Cache the location for 5 minutes, timeout after 6 seconds
    navigator.geolocation.getCurrentPosition(success, fail, {maximumAge: 500000, enableHighAccuracy: true, timeout: 6000});
} else {
    drawMap(sourceLatLng, posts);  // No geolocation support, show default map
}

function loadURL(marker) {
    return function () {
        window.location.href = marker.url;
    }
}

function drawMap(slatlng, tlatlon) {
    var myOptions = {
        zoom: 17,
        center: slatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        }
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

    // Add an overlay to the map of current lat/lng
    var sourceMarket = new google.maps.Marker({
        position: slatlng,
        icon: icons.info.icon,
        map: map,
        title: "Greetings!"
    });

    if (sourceRadio > 0) {
        var circle = new google.maps.Circle({
            map: map,
            radius: sourceRadio, // 10 miles in metres
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#FF0000',
            fillOpacity: 0.2
        });
        circle.bindTo('center', sourceMarket, 'position');
    }

    var sourceContentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + sourceHeader1 + '</h1>' +
//            '<div id="bodyContent">' +
//            '<p>' + sourceHeader2 + '</p>' +
//            '<p>' + sourceHeader3 + '</p>' +
//            '</div>' +
            '</div>';
    var sourceInfowindow = new google.maps.InfoWindow({
        content: sourceContentString
    });
    google.maps.event.addListener(sourceMarket, 'click', function () {
        sourceInfowindow.open(map, sourceMarket);
    });



    for (i = 0, l = tlatlon.length; i < l; i++) {

        var targetMarker = new google.maps.Marker({
            position: new google.maps.LatLng(tlatlon[i].lat, tlatlon[i].lon),
//            icon: icons.tree.icon,
            map: map,
            title: tlatlon[i].title,
            url: "/posts/" + tlatlon[i].id
        });

//        var targetContentString = '<div id="content">' +
//                '<div id="siteNotice">' +
//                '</div>' +
//                '<h1 id="firstHeading" class="firstHeading">' + tlatlon[i].title + '</h1>' +
//                '<div id="bodyContent">' +
//                '<p>' + tlatlon[i].title + '</p>' +
//                '<p>' + tlatlon[i].title + '</p>' +
//                '</div>' +
//                '</div>';
//        var targetInfowindow = new google.maps.InfoWindow({
//            content: targetContentString
//        });
//        google.maps.event.addListener(targetMarker, 'click', function () {
//            targetInfowindow.open(map, targetMarker);
//        });

        google.maps.event.addListener(targetMarker, 'click', loadURL(targetMarker));




    }

    google.maps.event.addDomListener(window, "resize", function () {
        var center = sourceLatLng;
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}


