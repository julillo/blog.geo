

if (locateClient && navigator.geolocation) {
    function success(pos) {
        // Location found, show map with these coordinates
        drawMap(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
    }

    function fail(error) {
        drawMap(defaultLatLng); // Failed to find location, show default map
    }

    // Find the users current position.  Cache the location for 5 minutes, timeout after 6 seconds
    navigator.geolocation.getCurrentPosition(success, fail, {maximumAge: 500000, enableHighAccuracy: true, timeout: 6000});
} else {
    drawMap(defaultLatLng); // No geolocation support, show default map
}

function drawMap(latlng) {
    var myOptions = {
        zoom: 17,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE
        }
    };
    var map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
    // Add an overlay to the map of current lat/lng
    var marker = new google.maps.Marker({
        position: latlng,
        map: map,
        title: "Greetings!"
    });
    if (radio > 0) {
        var circle = new google.maps.Circle({
            map: map,
            radius: radio, // 10 miles in metres
            strokeColor: '#FF0000',
            strokeOpacity: 0.5,
            strokeWeight: 1,
            fillColor: '#FF0000',
            fillOpacity: 0.2
        });
        circle.bindTo('center', marker, 'position');
    }

    var contentString = '<div id="content">' +
            '<div id="siteNotice">' +
            '</div>' +
            '<h1 id="firstHeading" class="firstHeading">' + header + '</h1>' +
            '<div id="bodyContent">' +
            '<p>' + subhead1 + '</p>' +
            '<p>' + subhead2 + '</p>' +
            '</div>' +
            '</div>';
    var infowindow = new google.maps.InfoWindow({
        content: contentString
    });
    google.maps.event.addListener(marker, 'click', function () {
        infowindow.open(map, marker);
    });
    google.maps.event.addDomListener(window, "resize", function () {
        var center = defaultLatLng;
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}


