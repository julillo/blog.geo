/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var options = {
    enableHighAccuracy: true,
    timeout: 5000,
    maximumAge: 0
};

//object = document.getElementById("form");
//object.addEventListener("submit", onSubmit);

//function onSubmit() {
//    var iLat = document.getElementById('lat');
//    if (iLat.value === "0") {
//        getLocation();
//        return false;
//    }
//}

$("form").submit(function(event){
    var iLat = document.getElementById('lat');
    if (iLat.value === "0") {
        getLocation();
        event.preventDefault();
//        return;
    }
    
});

function recordPosition(position) {
    var iLat = document.getElementById('lat');
    var iLong = document.getElementById('lon');
    iLat.value = position.coords.latitude;
    iLong.value = position.coords.longitude;
    iLat.form.submit();
}

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(getLocation_success, getLocation_error, options);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function getLocation_success(pos) {
//    showPosition(pos);
    recordPosition(pos);
}

function getLocation_error(err) {
    console.warn('ERROR(' + err.code + '): ' + err.message);
}