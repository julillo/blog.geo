<div id="map-canvas"></div>
<script src="https://maps.google.com/maps/api/js"></script>
<script type="text/javascript">
    var locateClient = false;
    var lat = {{ $post->lat }};
    var lon = {{ $post->lon }};
    var header = "{{ $post->title }}";
    var subhead1 = "{{ $post->user->name . ' on ' . $post->created_at->toFormattedDateString() }}";
    var subhead2 = "{{ $post->lat . ', ' . $post->lon }}";
    var radio = 0;
    var defaultLatLng = new google.maps.LatLng(lat, lon); // Default to Hollywood, CA when no geolocation support
</script>
<script type="text/javascript" src="/js/map_sp.js"></script>
