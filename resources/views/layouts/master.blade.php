<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../../favicon.ico">
        <title>Blog Template for Bootstrap</title>
        @include ("layouts.css")
        @yield ("stylesheets")
    </head>
    <body>
        @include ("layouts.masthead")
        @include ("layouts.header")
        <div class="container">
            <div class="row">
                @yield ("content")
                @include ("layouts.sidebar")
            </div><!-- /.row -->
        </div><!-- /.container -->
        @include ("layouts.footer")
        @include ("layouts.scripts")
        @yield("scripts")
    </body>
</html>

