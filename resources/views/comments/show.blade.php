<div class="comments">
    <ul class="list-group">
        @if(count($post->comments))
        @foreach($post->comments as $comment)
        <li class="list-group-item">
            <strong>
                {{ $comment->created_at->diffForHumans() }}: &nbsp;
            </strong>
            {{ $comment->body }}
        </li>
        @endforeach
        @else 
        <li class="list-group-item">
            No Comments.
        </li>
        @endif
    </ul>
</div>