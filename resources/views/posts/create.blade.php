@extends("layouts.master")
@section("content")
<div class="col-sm-8 blog-main">
    <h1>Publish a Post</h1>
    <hr>
    <form method="POST" action="/posts" id="form" enctype="multipart/form-data">
        <!--<form method="POST" action="/posts" onsubmit="onSubmit()">-->
        {{csrf_field()}}
        <input type="hidden" id="lat" name="lat" value="0">
        <input type="hidden" id="lon" name="lon" value="0">
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" class="form-control" id="title" name="title" required>
            <!--<input type="text" class="form-control" id="title" name="title" required>-->
        </div>
        <div class="form-group">
            <label for="body">Body</label>
            <textarea id="body" name="body" class="form-control" required></textarea>
            <!--<textarea id="body" name="body" class="form-control" required></textarea>-->
        </div>
        <div class="form-group">
            <label for="file">File</label>
            <input type="file" class="form-control" id="file" name="file" >
            <!--<textarea id="body" name="body" class="form-control" required></textarea>-->
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Publish</button>
            <!--<button type="button" class="btn btn-primary" onclick="javascript:onSubmit();">Publish</button>-->
        </div>
        @include ("layouts.errors")
    </form>
</div>
@endsection
@section("scripts")
<script type="text/javascript" src="/js/post.js"></script>
@endsection
@section("stylesheets")
<!--<link href="/css/app1.css" rel="stylesheet">-->
@endsection
