@extends ("layouts.master")

@section("content")
<div class="col-sm-8 blog-main">
    @include ("layouts.map_mp")
</div>
@endsection

@section("stylesheets")
<link href="/css/map.css" rel="stylesheet">
@endsection