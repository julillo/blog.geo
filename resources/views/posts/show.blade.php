@extends("layouts.master")
@section("content")
<div class="col-sm-8 blog-main">
    <h1>{{$post->title}}</h1>
    {{$post->body}}
    <hr>
    @include ("layouts.map_sp")
    <hr>
    <!--<img src="{{ Storage::url($post->postFiles[0]->filename)}}">-->
    <a href="{{ Storage::url($post->postFiles[0]->filename)}}">Archivo adjunto</a>
    <hr>
    @include ("comments.create")
    <hr>
    @include ("comments.show")
</div>
@endsection
@section("stylesheets")
<link href="/css/map.css" rel="stylesheet">
@endsection