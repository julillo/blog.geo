<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

class Comment extends Model {

    protected $fillable = ["body", "post_id", "user_id"]; //like a whitelist
    protected $guarded = []; //like a blacklist

    public function post() {
        return $this->belongsTo(Post::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

}
