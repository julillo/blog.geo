<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use \App\PostFile;
use App\Repositories\Posts;

class PostsController extends Controller {

    public function __construct() {
        $this->middleware("auth")->except(["index", "show", "map"]);
    }

    public function index(Posts $posts) {
        
//        dd($posts);

        $posts = $posts->all();
        
//        $posts = (new \App\Repositories\Posts)->all();
        
        
//        $posts = Post::latest()
//                ->filter(request(["month", "year"]))
//                ->get();

        return view("posts.index")->with("posts", $posts);
    }

    public function show(Post $post) {

        return view("posts.show", compact("post"));
    }

    public function create() {
        return view("posts.create");
    }

    public function store() {

        $this->validate(request(), [
            "title" => "required|max:191"
            , "body" => "required"
        ]);
        
        $file = request()->file('file')->store('public', 'local');
        
//        dd($something);

        auth()->user()->publish(
                new Post(request(["title", "body", "lat", "lon"]))
                , new PostFile(["filename" => $file])
        );

        return redirect("/");
    }
    
    public function map(Posts $posts){
        return view("posts.map")->with("posts", $posts->all());
//        return view("posts.map")->with("posts", Post::with("user")->get());
    }
}
