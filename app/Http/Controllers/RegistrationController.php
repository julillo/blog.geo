<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class RegistrationController extends Controller {
    
    public function __construct() {
        $this->middleware("guest"); //->except(["create"]);
    }
    
    public function create() {
        return view("registration.create");
    }

    public function store() {
        // validate the form

        $this->validate(request(), [
            "name" => "required|max:191"
            , "email" => "required|email|max:191"
            , "password" => "required|confirmed"
        ]);

        //create and save the user

        $user = User::create([
                    "name" => request("name")
                    , "email" => request("email")
                    , "password" => Hash::make(request("password"))
        ]);

        //sign them in

        auth()->login($user);

        //redirect to the home page

        return redirect()->home();
    }

}
