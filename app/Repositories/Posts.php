<?php

namespace App\Repositories;

use App\Post;
use App\Redis;

class Posts{
    
    protected $radis;
    
    public function __construct(Redis $redis) {
        $this->radis = $redis;
    }
    
    public function all() {
//        return Post::all();
        return Post::latest()
                ->filter(request(["month", "year"]))
                ->get();
    }
    
}
