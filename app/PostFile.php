<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostFile extends Model
{
    protected $fillable = ["post_id", "filename"]; //like a whitelist
    protected $guarded = []; //like a blacklist
    
    public function post() {
        return $this->belongsTo(Post::class);
    }
}
